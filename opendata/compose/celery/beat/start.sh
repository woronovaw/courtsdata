#!/bin/bash

set -o errexit
set -o nounset

rm -f '/tmp/celerybeat.pid'
celery -A config beat -l INFO --pidfile="/tmp/celerybeat.pid"