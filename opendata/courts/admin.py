from django.contrib import admin

from .models import Court, CourtCase


@admin.register(Court)
class CourtAdmin(admin.ModelAdmin):
    list_display = ("name", "site_link")
    search_fields = ["name"]


@admin.register(CourtCase)
class CaseAdmin(admin.ModelAdmin):
    list_filter = ("court__name",)
    search_fields = ["judge", "number"]
