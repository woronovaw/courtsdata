from django.contrib import admin
from django.db import models
from django.utils.html import format_html


class Court(models.Model):
    name = models.CharField(max_length=254)
    url = models.URLField()
    code = models.CharField(max_length=10)
    last_modified = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name

    @admin.display
    def site_link(self):
        return format_html('<a href="{0}">{0}</a>', self.url)


class CourtCase(models.Model):
    date = models.DateTimeField()
    judge = models.CharField(max_length=254)
    forma = models.TextField()
    number = models.CharField(max_length=254)
    involved = models.TextField()
    description = models.TextField()
    add_address = models.CharField(max_length=254)
    courtroom = models.CharField(max_length=254)
    court = models.ForeignKey(Court, on_delete=models.CASCADE)
    last_modified = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f"{self.number} at {self.date} - {self.judge}: {self.involved}"
