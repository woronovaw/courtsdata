# Generated by Django 3.2.4 on 2021-06-16 23:24

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('courts', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='court',
            name='last_modified',
            field=models.DateTimeField(auto_now=True),
        ),
    ]
