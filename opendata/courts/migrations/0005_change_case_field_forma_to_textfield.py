# Generated by Django 3.2.4 on 2021-06-17 22:43

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('courts', '0004_change_case_field_involved_to_textfield'),
    ]

    operations = [
        migrations.AlterField(
            model_name='case',
            name='forma',
            field=models.TextField(),
        ),
    ]
