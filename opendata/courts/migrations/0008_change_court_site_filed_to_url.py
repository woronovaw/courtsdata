# Generated by Django 3.2.4 on 2021-06-20 21:20

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('courts', '0007_rename_case_model'),
    ]

    operations = [
        migrations.RenameField(
            model_name='court',
            old_name='site',
            new_name='url',
        ),
    ]
