from celery import shared_task
from django.core.management import call_command


@shared_task
def update_or_save_courts():
    call_command("update_or_save_courts")


@shared_task
def update_or_save_cases():
    call_command("update_or_save_cases")
