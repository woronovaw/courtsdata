from asgiref.sync import sync_to_async
from django.shortcuts import render

from .models import Court


async def index(request):
    courts_list = await sync_to_async(list)(Court.objects.all())
    context = {"courts_list": courts_list}
    return render(request, "courts/index.html", context)
