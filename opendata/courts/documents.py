from django_elasticsearch_dsl import Document, fields
from django_elasticsearch_dsl.registries import registry

from .models import Court, CourtCase


@registry.register_document
class CourtCaseDocument(Document):
    court = fields.ObjectField(
        properties={
            "id": fields.IntegerField(),
            "name": fields.TextField(),
        },
    )

    class Index:
        name = "cases"
        settings = {"number_of_shards": 1, "number_of_replicas": 0}

    class Django:
        model = CourtCase

        fields = [
            "judge",
            "forma",
            "involved",
            "description",
            "add_address",
            "courtroom",
        ]

    def get_queryset(self):
        return super().get_queryset().select_related("court")

    def get_instances_from_related(self, related_instance):
        if isinstance(related_instance, Court):
            return related_instance.court_case.all()
