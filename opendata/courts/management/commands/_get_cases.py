import asyncio
import logging
import re
from collections import namedtuple
from datetime import datetime, timezone
from operator import itemgetter
from typing import Iterable, List, Tuple
from zoneinfo import ZoneInfo

from asgiref.sync import sync_to_async
from django.conf import settings
from pyppeteer import launch
from pyppeteer.browser import Browser
from pyppeteer.errors import TimeoutError
from pyppeteer.network_manager import Response
from pyppeteer.page import Page

from courts.models import Court, CourtCase


logger = logging.getLogger(settings.GET_CASES_LOGGER)

ResultStats = namedtuple("ResultStats", "created not_changed no_case")

SLEEP_BEFORE_RETRY = 25
CASES_ROW_MENU_RE = re.compile(
    r"^\s*список\s*\w*\s*справ.\s*призначених.*розгляду\s*$", re.I
)


def prepare_date(raw_date: str) -> datetime:
    if len(raw_date) == 10:
        date = datetime.strptime(raw_date, "%d.%m.%Y")
    else:
        date = datetime.strptime(raw_date, "%d.%m.%Y %H:%M")
    date = date.astimezone(ZoneInfo("Europe/Kiev"))
    return date.replace(tzinfo=timezone.utc)


@sync_to_async
def get_all_courts() -> Iterable[Iterable[Court]]:
    courts_list = list(Court.objects.all())
    logger.info(str(len(courts_list)))
    return courts_list


@sync_to_async
def save_new_cases(
    cases: List[dict], court: Court, event: asyncio.Event,
) -> Tuple[int, int]:
    created_num = 0
    not_changed_num = 0

    for case in cases:
        date = prepare_date(case.pop("date"))
        _, created = CourtCase.objects.get_or_create(
            **case, date=date, court=court,
        )
        if created:
            created_num += 1
        else:
            not_changed_num += 1

    event.set()
    return created_num, not_changed_num


async def interception_function(
    response: Response,
    event: asyncio.Event,
    result_stats: List[ResultStats],
    court: Court,
) -> None:
    if "new.php" in response.url:
        try:
            cases = await response.json()
        except Exception:
            logger.info(
                "%s (%s) court site has an error in the data",
                court.name,
                court.code,
            )
            result_stats.append(ResultStats(0, 0, 1))
            event.set()
            return

        if cases:
            created_num, not_changed_num = await save_new_cases(
                cases, court, event
            )
            result_stats.append(ResultStats(created_num, not_changed_num, 0))
            logger.info(
                "%s, successfully created %s and didn't change %s cases",
                court.name,
                created_num,
                not_changed_num,
            )
        else:
            logger.warning("Court %s(%s) has not data", court.name, court.code)
            result_stats.append(ResultStats(0, 0, 1))

        event.set()


async def cleardate_or_notinfo(
    page: Page, event: asyncio.Event, court: Court,
) -> None:
    url = f"https://gl.ki.court.gov.ua/{court.code}"
    try:
        await page.goto(url, waitUntil="networkidle0")
    except TimeoutError:
        logger.warning("Timeout error on %s (%s)", url, court.name)
        await asyncio.sleep(SLEEP_BEFORE_RETRY)
        await page.goto(url, waitUntil="networkidle0")

    menu_rows = await page.JJeval(
        ".dropdown-menu ol a",
        "(nodes => nodes.map(n => [n.innerText, n.href]))",
    )
    menu_item_text = itemgetter(0)
    menu_item_href = itemgetter(1)
    cases_url = [
        r for r in menu_rows if CASES_ROW_MENU_RE.search(menu_item_text(r))
    ]
    if not cases_url:
        await page.screenshot(
            {"path": settings.ERRORS_DIR / f"no_case_list_{court.code}.png"}
        )
        event.set()
        return

    await page.goto(menu_item_href(cases_url[0]), waitUntil="networkidle0")
    cleardate = await page.J("#cleardate")
    notinfo = await page.J("#notinfo")
    if not cleardate and not notinfo:
        event.set()
        return
    if cleardate:
        await cleardate.click()
        return
    if notinfo:
        await page.screenshot(
            {"path": settings.ERRORS_DIR / f"notinfo_{court.code}.png"}
        )
        event.set()


async def get_court_cases(
    semaphore: asyncio.Semaphore,
    browser: Browser,
    result_stats: List[ResultStats],
    court: Court,
) -> None:
    await semaphore.acquire()
    event = asyncio.Event()
    page = await browser.newPage()
    await page._client.send(
        "Network.enable",
        dict(
            maxResourceBufferSize=1024 * 1204 * 100,
            maxTotalBufferSize=1024 * 1204 * 200,
        ),
    )
    page.on(
        "response",
        lambda response: asyncio.create_task(
            interception_function(response, event, result_stats, court)
        ),
    )
    await cleardate_or_notinfo(page, event, court)
    await event.wait()
    await page.close()
    semaphore.release()


async def get_courts_cases() -> None:
    result_stats: List[ResultStats] = []
    browser = await launch(
        args=["--no-sandbox"],
        handleSIGINT=False,
        handleSIGTERM=False,
        handleSIGHUP=False,
    )
    semaphore = asyncio.Semaphore(value=10)
    await asyncio.gather(
        *[
            get_court_cases(semaphore, browser, result_stats, court)
            for court in await get_all_courts()
        ]
    )
    logger.info("Handled %d courts", len(result_stats))
    logger.info(
        "%d courts don't have cases", sum(r.no_case for r in result_stats)
    )
    logger.info(
        "Created %d and  didn't change %d cases",
        sum(r.created for r in result_stats),
        sum(r.not_changed for r in result_stats),
    )
    await browser.close()
