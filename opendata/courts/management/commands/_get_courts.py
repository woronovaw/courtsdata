import asyncio
import logging
from collections import namedtuple
from operator import itemgetter
from typing import AsyncGenerator, List, Optional, Tuple

from asgiref.sync import sync_to_async
from django.conf import settings
from pyppeteer import launch
from pyppeteer.browser import Browser
from pyppeteer.errors import TimeoutError

from courts.models import Court

logger = logging.getLogger(settings.GET_COURTS_LOGGER)

COURT_NAME = '.Cname'
COURTS_URL = "https://court.gov.ua/sudova-vlada/sudy/"
DIV_FORMS = "body > div.container.content > \
    div.court-blocks > div.row.court-blocks__row"

DIV_FORMS_GENERAL = DIV_FORMS + " > div:nth-child(1)"
DISTRICT_OF_CITY_FORM = DIV_FORMS_GENERAL + " > form:nth-child(14)"
DISTRICT_OF_CITY_REGION = (
    DISTRICT_OF_CITY_FORM + " > div:nth-child(3) > select"
)
DISTRICT_OF_CITY_CITY = "#select4"
DISTRICT_OF_CITY_DISTRICT = "#select5"
DISTRICT_OF_CITY_BUTTON = (
    DISTRICT_OF_CITY_FORM + " > div:nth-child(5) > button"
)

CITY_FORM = DIV_FORMS_GENERAL + " > form:nth-child(12)"
CITY_REGION = CITY_FORM + " > div:nth-child(3) > select"
CITY_DISTRICT = "#select3"
CITY_BUTTON = CITY_FORM + " > div:nth-child(4) > button"

CITY_DISTRICT_FORM = DIV_FORMS_GENERAL + " > form:nth-child(10)"
CITY_DISTRICT_REGION = CITY_DISTRICT_FORM + " > div:nth-child(3) > select"
CITY_DISTRICT_DISTRICT = "#select2"
CITY_DISTRICT_BUTTON = CITY_DISTRICT_FORM + " > div:nth-child(4) > button"

DISTRICT_FORM = DIV_FORMS_GENERAL + " > form:nth-child(8)"
DISTRICT_REGION = DISTRICT_FORM + " > div:nth-child(3) > select"
DISTRICT_DISTRICT = "#select1"
DISTRICT_BUTTON = DISTRICT_FORM + " > div:nth-child(4) > button"

APELAC_FORM = DIV_FORMS_GENERAL + " > form:nth-child(4)"
APELAC_REGION = APELAC_FORM + " > div > select"
APELAC_BUTTON = APELAC_FORM + " > div > button"

COMERCIAL_FORM = DIV_FORMS + " > div:nth-child(2)"
COMERCIAL_CITY_FORM = COMERCIAL_FORM + " > form:nth-child(8)"
COMERCIAL_CITY_REGION = COMERCIAL_CITY_FORM + " > div > select"
COMERCIAL_CITY_BUTTON = COMERCIAL_CITY_FORM + " > div > button"

COMERCIAL_APPELLATE_FORM = COMERCIAL_FORM + " > form:nth-child(4)"
COMERCIAL_APPELLATE_REGION = COMERCIAL_APPELLATE_FORM + " > div > select"
COMERCIAL_APPELLATE_BUTTON = COMERCIAL_APPELLATE_FORM + " > div > button"

ADMIN_FORM = DIV_FORMS + " > div:nth-child(3)"
ADMIN_CITY_FORM = ADMIN_FORM + " > form:nth-child(8)"
ADMIN_CITY_REGION = ADMIN_CITY_FORM + " > div > select"
ADMIN_CITY_BUTTON = ADMIN_CITY_FORM + " > div > button"

ADMIN_FORM = DIV_FORMS + " > div:nth-child(3)"
ADMIN_APPELLATE_FORM = ADMIN_FORM + " > form:nth-child(4)"
ADMIN_APPELLATE_REGION = ADMIN_APPELLATE_FORM + " > div > select"
ADMIN_APPELLATE_BUTTON = ADMIN_APPELLATE_FORM + " > div > button"


Selection = namedtuple("Selection", "region place district")
CourtRow = namedtuple("CortRow", "name url code")


@sync_to_async
def create_or_update_courts(
    courts: List[Optional[CourtRow]],
) -> Tuple[int, int]:
    created_num = 0
    updated_num = 0

    for court in courts:
        if court:
            _, created = Court.objects.update_or_create(
                code=court.code,
                defaults={"name": court.name, "url": court.url},
            )
            if created:
                created_num += 1
                logger.info("Created record: %s (%s)", court.name, court.url)
            else:
                updated_num += 1
                logger.info("Updated record: %s (%s)", court.name, court.url)

    return created_num, updated_num


async def get_court_options(
    browser: Browser,
    form_selector: str,
    reg_selector: str,
    city_selector: Optional[str] = None,
    distr_selector: Optional[str] = None,
) -> AsyncGenerator[Selection, None]:
    page = await browser.newPage()
    await page.goto(COURTS_URL)
    form = await page.J(form_selector)
    if form is not None:
        regions = await form.JJeval(
            f"{reg_selector} option", "(nodes) => nodes.map(n => n.value)"
        )
        for region in regions:
            await page.select(reg_selector, str(region))
            if city_selector is None:
                yield Selection(region, None, None)
            else:
                cities = await form.JJeval(
                    f"{city_selector} option",
                    "(nodes) => nodes.map(n => n.value)",
                )
                for city in cities:
                    await page.select(city_selector, str(city))
                    if distr_selector is None:
                        yield Selection(region, city, None)
                    else:
                        districts = await form.JJeval(
                            f"{distr_selector} option",
                            "(nodes) => nodes.map(n => n.value)",
                        )
                        for district in districts:
                            await page.select(distr_selector, str(district))
                            yield Selection(region, city, district)
    else:
        logger.error(
            '"%s" page doesn\'t have form at the selector: "%s"',
            COURTS_URL,
            form_selector,
        )
    await page.close()


async def get_courts_row(
    semaphore: asyncio.Semaphore,
    browser: Browser,
    sel: Selection,
    reg_selector: str,
    button_selector: str,
    city_selector: Optional[str],
    distr_selector: Optional[str],
) -> Optional[CourtRow]:
    await semaphore.acquire()
    page = await browser.newPage()
    await page.goto(COURTS_URL)
    await page.select(reg_selector, sel.region)
    if city_selector:
        await page.select(city_selector, sel.place)
    if distr_selector:
        await page.select(distr_selector, sel.district)

    await page.waitForSelector(button_selector)
    button = await page.J(button_selector)
    if button:
        await button.click()

        try:
            await page.waitForSelector(COURT_NAME)
        except TimeoutError:
            file_name = f"error_{sel.region}_{sel.place}_{sel.district}.png"
            await page.screenshot({
                "path": settings.ERRORS_DIR / file_name,
                "fullPage": True,
            })
            await page.close()
            logger.error(
                'On "%s" page was raised TimeoutError\
                while waiting "%s" selector, %s',
                COURTS_URL,
                COURT_NAME,
                sel,
            )
            semaphore.release()
            return None
        else:
            court_data = await page.JJeval(
                f"{COURT_NAME} > a",
                "(nodes => nodes.map(n => [n.innerText, n.href]))",
            )
            await page.close()
            name, url = court_data[0]
            get_code = itemgetter(-2)
            semaphore.release()
            return CourtRow(name, url, get_code(url.split("/")))

    logger.error(
        '"%s" page doesn\'t have form\'s button at the selector: "%s"',
        COURTS_URL,
        button_selector,
    )
    await page.close()
    semaphore.release()
    return None


async def create_court_tasks(
    semaphore: asyncio.Semaphore,
    browser: Browser,
    form_selector: str,
    button_selector: str,
    reg_selector: str,
    city_selector: Optional[str] = None,
    distr_selector: Optional[str] = None,
):
    return [
        get_courts_row(
            semaphore,
            browser,
            sel,
            reg_selector,
            button_selector,
            city_selector,
            distr_selector,
        )
        async for sel in get_court_options(
            browser,
            form_selector,
            reg_selector,
            city_selector,
            distr_selector,
        )
    ]


async def save_courts() -> Tuple[int, int]:
    browser = await launch(
        args=["--no-sandbox"],
        handleSIGINT=False,
        handleSIGTERM=False,
        handleSIGHUP=False,
    )
    created_num = 0
    updated_num = 0
    semaphore = asyncio.Semaphore(value=10)

    tasks_list = []
    tasks_list.extend(await create_court_tasks(
        semaphore,
        browser,
        DISTRICT_OF_CITY_FORM,
        DISTRICT_OF_CITY_BUTTON,
        DISTRICT_OF_CITY_REGION,
        DISTRICT_OF_CITY_CITY,
        DISTRICT_OF_CITY_DISTRICT,
    ))
    tasks_list.extend(await create_court_tasks(
        semaphore, browser, CITY_FORM, CITY_BUTTON, CITY_REGION, CITY_DISTRICT,
    ))
    tasks_list.extend(await create_court_tasks(
        semaphore,
        browser,
        CITY_DISTRICT_FORM,
        CITY_DISTRICT_BUTTON,
        CITY_DISTRICT_REGION,
        CITY_DISTRICT_DISTRICT,
    ))
    tasks_list.extend(await create_court_tasks(
        semaphore,
        browser,
        DISTRICT_FORM,
        DISTRICT_BUTTON,
        DISTRICT_REGION,
        DISTRICT_DISTRICT,
    ))
    tasks_list.extend(await create_court_tasks(
        semaphore, browser, APELAC_FORM, APELAC_BUTTON, APELAC_REGION,
    ))
    tasks_list.extend(await create_court_tasks(
        semaphore,
        browser,
        COMERCIAL_APPELLATE_FORM,
        COMERCIAL_APPELLATE_BUTTON,
        COMERCIAL_APPELLATE_REGION,
    ))
    tasks_list.extend(await create_court_tasks(
        semaphore,
        browser,
        COMERCIAL_CITY_FORM,
        COMERCIAL_CITY_BUTTON,
        COMERCIAL_CITY_REGION,
    ))
    tasks_list.extend(await create_court_tasks(
        semaphore,
        browser,
        ADMIN_APPELLATE_FORM,
        ADMIN_APPELLATE_BUTTON,
        ADMIN_APPELLATE_REGION,
    ))
    tasks_list.extend(await create_court_tasks(
        semaphore,
        browser,
        ADMIN_CITY_FORM,
        ADMIN_CITY_BUTTON,
        ADMIN_CITY_REGION,
    ))

    courts = await asyncio.gather(*tasks_list)
    created, updated = await create_or_update_courts(courts)
    created_num += created
    updated_num += updated

    await browser.close()
    return created_num, updated_num
