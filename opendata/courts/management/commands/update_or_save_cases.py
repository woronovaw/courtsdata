from asgiref.sync import async_to_sync
from django.core.management.base import BaseCommand

from ._get_cases import get_courts_cases


class Command(BaseCommand):
    help = "Gets court cases data and save or update it"

    def handle(self, *args, **options) -> None:
        async_to_sync(get_courts_cases)()
        self.stdout.write(
            self.style.SUCCESS("Successfully updeted court cases")
        )
