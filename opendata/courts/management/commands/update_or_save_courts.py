import logging

from asgiref.sync import async_to_sync
from django.conf import settings
from django.core.management.base import BaseCommand

from ._get_courts import save_courts

logger = logging.getLogger(settings.GET_COURTS_LOGGER)


class Command(BaseCommand):
    help = "Gets courts data and save or update it"

    def handle(self, *args, **options) -> None:
        created_num, updated_num = async_to_sync(save_courts)()
        self.stdout.write(
            self.style.SUCCESS(
                "Successfully created %d and updeted %d courts"
                % (created_num, updated_num),
            ),
        )
