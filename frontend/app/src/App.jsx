import React, { useState, useEffect } from 'react'
import logo from './logo.svg'
import './App.css'

const courts = [{name: 'Court 1', url: "https://test.com"}, {name: 'Court 2', url: "https://test.com"}]


const CourtRow = ({name, url}) => (
  <a href={url}>{name}</a>
)


function App() {
  const [value, setValue] = useState('');

  const handleChange = (value) => {
    setValue(value);
  }

  return (
    <div className="App">
        {courts.map(c => (<CourtRow name={c.name} url={c.url} key={c.name}/>))}
    </div>
  )
}

export default App
